﻿using System;
using UnityEngine;

namespace Rayark.Hi
{
    public class SwipeInputHandler : MonoBehaviour
    {
        public event Action<Vector2> OnSwipe;

        private Vector2 m_touchBeginPos;
        private Vector3 m_mouseBeginPos;

        // Update is called once per frame
        void Update()
        {
#if UNITY_ANDROID || UNITY_IOS
            this.TouchSwipe();
#else
            this.MouseSwipe();
#endif
        }

        /// <summary>
        /// 觸控式拖拉
        /// </summary>
        private void TouchSwipe()
        {
            // Handle screen touches.
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Began)
                {
                    m_touchBeginPos = touch.position;
                }

                if (touch.phase == TouchPhase.Ended)
                {
                    Vector2 swipeDiff = touch.position - m_touchBeginPos;
                    //Debug.Log("Swipe: " + swipeDiff);
                    this.OnSwipe?.Invoke(swipeDiff);
                }
            }
        }
        /// <summary>
        /// 滑鼠拖拉
        /// </summary>
        private void MouseSwipe()
        {
            if(Input.GetMouseButtonDown(0) )
            {
                this.m_mouseBeginPos = Input.mousePosition;
            }
            if (Input.GetMouseButtonUp(0))
            {
                Vector3 swipeDiff = Input.mousePosition - m_mouseBeginPos;
                //Debug.Log("Swipe: " + swipeDiff);
                this.OnSwipe?.Invoke(swipeDiff);
            }
        }
    }
}