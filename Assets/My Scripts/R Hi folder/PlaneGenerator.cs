﻿using UnityEngine;

namespace Rayark.Hi
{
    public class PlaneGenerator : MonoBehaviour
    {
        public const float PLANE_MAX_X = 1.6f;
        public const float PLANE_MIN_X = -1.6f;
        private const float PLANE_INTERVAL_Z = 10f;

        [SerializeField]
        private GameObject m_planePrefab;
        [SerializeField]
        private int m_iPlaneCount;
        [SerializeField]
        private Transform m_planeRootTransform;

        private GameObject[] m_arrPlanesInScene;
        private int m_iCurrentPlaneIndex = 0;

        // Start is called before the first frame update
        void Start()
        {
            m_arrPlanesInScene = new GameObject[m_iPlaneCount];
            for (int i = 0; i < m_iPlaneCount; i++)
            {
                Vector3 spawnPos = new Vector3(
                    m_planeRootTransform.transform.position.x,
                m_planeRootTransform.transform.position.y,
                m_planeRootTransform.transform.position.z + PLANE_INTERVAL_Z * i);
                m_arrPlanesInScene[i] = Instantiate(m_planePrefab);
                m_arrPlanesInScene[i].transform.position = spawnPos;

                if(i % 5 == 4)
                {
                    MaterialPropertyBlock propBlock = new MaterialPropertyBlock();
                    MeshRenderer rendererTemp = m_arrPlanesInScene[i].GetComponent<MeshRenderer>();
                    rendererTemp.GetPropertyBlock(propBlock);
                    propBlock.SetColor("_Color", Color.blue);
                    rendererTemp.SetPropertyBlock(propBlock);
                }
            }
        }

       public void UpdatePlanes(float _fCharacterPosZ)
        {
            int iNextPlaneIndex = this.GetNextPlaneIndex();

            if(_fCharacterPosZ > m_arrPlanesInScene[iNextPlaneIndex].transform.localPosition.z)
            {
                var currentPlane = m_arrPlanesInScene[m_iCurrentPlaneIndex];
                var lastPlane = m_arrPlanesInScene[this.GetPreviousPlaneIndex()];

                currentPlane.transform.localPosition = new Vector3(
                    currentPlane.transform.localPosition.x,
                    currentPlane.transform.localPosition.y,
                    lastPlane.transform.localPosition.z + PLANE_INTERVAL_Z);

                this.m_iCurrentPlaneIndex = this.GetNextPlaneIndex();
            }
        }

        private int GetNextPlaneIndex()
        {
            return this.GetNextPlaneIndex(m_iCurrentPlaneIndex);
        }
        private int GetPreviousPlaneIndex()
        {
            return this.GetPreviousPlaneIndex(m_iCurrentPlaneIndex);
        }

        public int GetNextPlaneIndex(int _iIndex)
        {
            return (_iIndex + 1) % m_iPlaneCount;
        }
        public int GetPreviousPlaneIndex(int _iIndex)
        {//不確定各程式語言對於負數取餘的做法，因此這邊先加到變正數再做%運算
            return (_iIndex - 1 + m_iPlaneCount) % m_iPlaneCount;
        }
    }
}