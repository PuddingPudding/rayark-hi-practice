﻿using UnityEngine;

namespace Rayark.Hi
{
    public class CharacterView : MonoBehaviour
    {
        public enum EAnimState
        {
            IDLE,
            RUN
        }

        [SerializeField]
        private Transform m_characterTransform;
        [SerializeField]
        private Animator m_anim;

        public Vector3 Position
        {
            get { return m_characterTransform.localPosition; }
            set { m_characterTransform.localPosition = value; }
        }

        public void PlayAnimation(EAnimState _state)
        {
            switch(_state)
            {
                case EAnimState.IDLE:
                    this.m_anim.Play("Idle");
                    break;
                case EAnimState.RUN:
                    this.m_anim.Play("Running");
                    break;
            }
        }
    }
}
