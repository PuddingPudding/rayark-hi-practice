﻿using UnityEngine;

namespace Rayark.Hi
{
    using Engine;
    using UnityEngine.UI;

    public class GamePlayMgr : MonoBehaviour
    {
        [SerializeField]
        private CharacterData m_characterData;
        [SerializeField]
        private float m_fScaleX;
        [SerializeField]
        private float m_fMaxDirX;
        [SerializeField]
        private SwipeInputHandler m_swipeHandler;
        [SerializeField]
        private CharacterView m_characterView;
        [SerializeField]
        private Transform m_cameraTransform;
        [SerializeField]
        private float m_fCameraOffsetZ = -2.5f;
        [SerializeField]
        private PlaneGenerator m_planeGenerator;
        [SerializeField]
        private Text m_swipeRemainTxt;

        private HiEngine m_hiEngine;

        // Start is called before the first frame update
        void Start()
        {
            m_hiEngine = new HiEngine(m_characterData , m_fScaleX);
            m_swipeHandler.OnSwipe += this.SpeedUpCharacter;
            m_swipeRemainTxt.text = m_hiEngine.SwipeRemainCount.ToString();
        }
        private void OnDestroy()
        {
            m_swipeHandler.OnSwipe -= this.SpeedUpCharacter;
        }

        // Update is called once per frame
        void Update()
        {
            m_hiEngine.Update(Time.deltaTime);

            var characterPos = m_hiEngine.CurrentCharacterPosition;
            m_characterView.Position = new Vector3(
                characterPos.x * (PlaneGenerator.PLANE_MAX_X - PlaneGenerator.PLANE_MIN_X) + PlaneGenerator.PLANE_MIN_X,
                m_characterView.Position.y,
                characterPos.y);

            if (m_hiEngine.CurrentCharacterSpeed > 0)
            {
                m_characterView.PlayAnimation(CharacterView.EAnimState.RUN);
            }
            else { m_characterView.PlayAnimation(CharacterView.EAnimState.IDLE); }

            m_cameraTransform.localPosition = new Vector3(
                m_cameraTransform.localPosition.x,
                m_cameraTransform.localPosition.y,
                characterPos.y +m_fCameraOffsetZ);
            m_planeGenerator.UpdatePlanes(characterPos.y);
        }

        private void SpeedUpCharacter(Vector2 _swipeDir)
        {
            //_swipeDir = _swipeDir.normalized;
            //_swipeDir.x = Mathf.Clamp(_swipeDir.x, -m_fMaxDirX, m_fMaxDirX);
            //實驗功能，改將滑動向量的X分量限制在一個範圍內
            
            if(m_hiEngine.SwipeRemainCount > 0)
            {
                m_hiEngine.SpeedUpCharacterSpeed();
                m_hiEngine.ChangeCharacterDir(_swipeDir);
                m_hiEngine.ReduceSwipeRemainCount();
                m_swipeRemainTxt.text = m_hiEngine.SwipeRemainCount.ToString();
            }
        }
    }
}