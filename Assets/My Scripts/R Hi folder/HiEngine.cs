﻿using UnityEngine;

namespace Rayark.Hi.Engine
{
    public class HiEngine
    {
        private const float MIN_SWIPE_LENGTH = 100;
        private const int SWIPE_INIT_COUNT = 5;
        private const float MIN_X_VALUE = 0;
        private const float MAX_X_VALUE = 1;

        private CharacterData m_currentCharacter;
        private float m_fScaleX;
        private int m_iSwipeRemainCount;
        private float m_fRunDistance;

        public Vector2 CurrentCharacterPosition
        {
            get { return m_currentCharacter.Position; }
        }
        public float CurrentCharacterSpeed
        {
            get { return m_currentCharacter.Speed; }
        }
        public int SwipeRemainCount { get { return this.m_iSwipeRemainCount; } }

        public HiEngine(CharacterData _characterData)
            : this(_characterData, 1) { }

        public HiEngine(CharacterData _characterData, float _fScaleX)
        {
            m_fScaleX = _fScaleX;
            m_currentCharacter = _characterData;
            this.m_iSwipeRemainCount = SWIPE_INIT_COUNT;
            m_fRunDistance = 0;
        }
        public void Update(float _fDeltaTime)
        {
            m_currentCharacter.Position += _fDeltaTime * m_currentCharacter.UnitDirection * m_currentCharacter.Speed;
            if(m_currentCharacter.Position.x <= MIN_X_VALUE ||
                m_currentCharacter.Position.x >= MAX_X_VALUE)
            {
                m_currentCharacter.UnitDirection.x = -m_currentCharacter.UnitDirection.x;
            }
            m_currentCharacter.Position.x = Mathf.Clamp(m_currentCharacter.Position.x, MIN_X_VALUE, MAX_X_VALUE);
            m_currentCharacter.Speed =
                (1 - m_currentCharacter.SpeedDownRatio * _fDeltaTime) * m_currentCharacter.Speed - m_currentCharacter.SpeedDownAmount * _fDeltaTime;
            m_currentCharacter.Speed = Mathf.Max(0, m_currentCharacter.Speed);
        }

        public void ReduceSwipeRemainCount()
        {
            m_iSwipeRemainCount--;
        }
        public void SpeedUpCharacterSpeed()
        {
            m_currentCharacter.Speed =
                m_currentCharacter.Speed * (1 + m_currentCharacter.SpeedUpRatio)
                + m_currentCharacter.SpeedUpAmount;
        }

        public void ChangeCharacterDir(Vector2 _dir)
        {
            Debug.Log("拖拉長度" + _dir.magnitude);
            if(_dir.magnitude < MIN_SWIPE_LENGTH)
            {
                Debug.Log("拖拉長度不足");
                return;
            }
            if(_dir.y < 0) { _dir.y = -_dir.y; }
            if(Mathf.Abs(_dir.x) > Mathf.Sqrt(3) * _dir.y)
            {
                _dir = new Vector2(_dir.x, Mathf.Abs(_dir.x / Mathf.Sqrt(3)));
            }

            _dir.x *= m_fScaleX;
            m_currentCharacter.UnitDirection = _dir.normalized;
        }
    }
}